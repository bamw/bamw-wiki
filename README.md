# BAMW-Wiki

## PIC compilation

### CCS compilation rules
CCS is a crazy compiler with strange rules:

* Only one C contating main() function should be given to compiler.
* The main file (or its first included header) must start with ```#include <PICDEVICE.h>```
* Other C files must be #included in the main C file.
* Compiler must be configured with search paths of Devices and Drivers, see below.

### Configure MPLAB X for CCS compiler
```
Tools > Plugins > Available Plugins > CCS C Compiler > Install > Restart Now
Options > Embedded > Build Tools > Add... > C:\Program Files (x86)\PICC_v4 > OK > OK
Project properties > CCS C Compiler > Include Directories > 
	> Browse c:\Program Files (x86)\PICC_v4\Drivers 
	> Browse c:\Program Files (x86)\PICC_v4\Devices
```    
If project files cannot be found during build add project directory too
    
	
### Configure Old MPLAB 8 for CCS compiler
```
Project > Set Language Tool Locations > CCS C Compiler > Executables >
    C:\Program Files (x86)\PICC_v4\Ccsc.exe

Project > Set Language Tool Locations > CCS C Compiler > Include Search Path >
    c:\Program Files (x86)\PICC_v4\Drivers;c:\Program Files (x86)\PICC_v4\Devices    
```

### CCS compilation problem after project was moved
* If you open old MPLAB project and it does not compile because its sources cannot be found
it is possible MCP project file contains broken paths.
* Close the project.
* Open MCP file in text editor and remove file paths leaving only file names.
* Try opening and building project again.
* If it does not help just leave along and do something else.


## Networking on Windows

### Set static route for 10.2.0.150
    via a gateway
        route add 10.2.0.150 mask 255.255.255.255 198.168.10.70
    without gateway via interface 27
        route add 10.2.0.150 mask 255.255.255.255 0.0.0.0 if 27
    permanent
        route -p ...

To find out the interface for **if** parameter
     route print | findstr "\.\.\.

## Git

### Configuration
```
git config --list
git config --global user.name ""
git config --global user.email ""
git config --global core.editor vim
git config --global core.pager "cat"
git config --global core.autocrlf true  
git config --global push.default upstream           #push to remote branch this local branch was created from
git config --global branch.autosetuprebase always   #always rebase when pulling to prevent merge commits created after pulling remote branch
git config branch.master.rebase true                #rebase while pulling master branch (if it was created before global options was set)

git config --global push.followTags true            #always push tags to remote 
git config --global credential.helper store         #remember repo password for 15 minutes
git config --global status.submoduleSummary true    #include submodules in status
git config --global diff.submodule log              #better submodule status with diff
git config --global format.pretty '%C(auto)%h%<(18,trunc) %ci %<(5,trunc)%ae %C(white bold)%s %C(auto)%d%C(reset)'
git config --global alias.loga "log --graph --all --date-order"
git config --global alias.dt "difftool"

#find pattern in repo
git config --global alias.gg '!f() { git grep --break -p -P -B1 -A2 --heading $1 $(git rev-list --all) $2; }; f'

#find pattern in repo, whole words, not binaries
git config --global alias.gg '!f() { git grep --break -p -P -I -w -B1 -A2 --heading $1 $(git rev-list --all) $2; }; f'

#tortoise difftool
git config --global --add difftool.prompt false
git config --global diff.tool TortoiseGitMerge
git config --global difftool.TortoiseGitMerge.path 'c:\Program Files\TortoiseGit\bin\TortoiseGitMerge.exe'
git config --global difftool.TortoiseGitMerge.cmd 'TortoiseGitMerge -mine "$REMOTE" -base "$LOCAL"'
git config --global merge.tool TortoiseGitMerge
git config --global mergetool.TortoiseGitMerge.path 'c:\Program Files\TortoiseGit\bin\TortoiseGitMerge.exe'
git config --global mergetool.TortoiseGitMerge.cmd 'TortoiseGitMerge -base "$BASE" -theirs "$REMOTE" -mine "$LOCAL" -merged "$MERGED"'

#meld difftool on Linux
git config --global --add difftool.prompt false
git config --global diff.tool meld
git config --global difftool.meld.cmd 'meld $LOCAL $REMOTE'
git config --global merge.tool meld
git config --global mergetool.meld.cmd 'meld $LOCAL $MERGED $REMOTE --output "$MERGED"'

#meld difftool on Windows
git config --global difftool.meld.path 'c:\Program Files (x86)\Meld\Meld.exe'
git config --global mergetool.meld.path 'c:\Program Files (x86)\Meld\Meld.exe'
```





[Markdown syntax](https://confluence.atlassian.com/bitbucketserver/markdown-syntax-guide-776639995.html)